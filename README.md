## Overview

Primary **Port 80**

**Deligating**

```
tripgeni.com, www.tripgeni.com - http://tripgeni.com:8080  
dashboard.tripgeni.com - http://tripgeni.com:8081  
test.tripgeni.com - http://tripgeni.com:8082  
api.tripgeni.com - http://tripgeni.com:8083  
public.tripgeni.com - http://tripgeni.com:8084  
cicd.tripgeni.com - http://tripgeni.com:8085  
admin.tripgeni.com - http://tripgeni.com:8087  
*.tripgeni.com - http://tripgeni.com:8086  
```

SSH to docker container - this is alpine linx so
```
docker exec -it react-backend-build-docker_nginx_proxy_1 sh
```

